#include <memory>
#include <thread>
#include <nanogui/common.h>
#include <nanogui/screen.h>
#include <nanogui/layout.h>
#include <nanogui/window.h>
#include <GLFW/glfw3.h>

#include "synth.h"
#include "ui/synth_view.h"
#include "ui/layouts/dock_layout.h"
#include "input/glfw_input_controller.h"

#include <iostream>

using namespace nanogui;
using gcaseres::synth::Synth;
using gcaseres::synth::input::GLFWInputController;
using gcaseres::synth::ui::SynthView;
using gcaseres::synth::ui::layouts::DockLayout;

GLFWInputController input_controller;

class Application : public nanogui::Screen {
 public:
  Application(std::shared_ptr<Synth> synth)
      : nanogui::Screen(Eigen::Vector2i(1024, 768), "Quarantine Synth") {
    this->synth_ = synth;

    nanogui::Window* window = new nanogui::Window(this);
   // this->setLayout(new DockLayout());
    window->setLayout(new DockLayout());
    window->setSize(Vector2i(500,500));
    new SynthView(synth, window);
  }

  virtual void drawAll() override {
    this->synth_->Update();
    input_controller.Update();
    Screen::drawAll();
  }

  ~Application() {}

  virtual bool resizeEvent(const Vector2i& size) { this->performLayout(); }

 private:
  std::shared_ptr<Synth> synth_;
};

static void KeyCallback(GLFWwindow* window, int key, int scancode, int action,
                        int mods) {
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GLFW_TRUE);

  input_controller.GLFWKeyCallback(window, key, scancode, action, mods);
}

bool run = false;

int main() {
  nanogui::init();

  {
    std::shared_ptr<Synth> synth = std::make_shared<Synth>();

    if (synth->Initialize(input_controller) < 0) {
      std::wcout << "ERROR: Couldn't initialize Synth" << std::endl;
      return -1;
    }

    nanogui::ref<Application> app(new Application(synth));

    run = true;
    /*
    std::thread synth_thread = std::thread([synth]() {
      while (run) {
        input_controller.Update();
        synth->Update();
      }
    });
*/
    glfwSetKeyCallback(app->glfwWindow(), KeyCallback);
    app->performLayout();
    app->drawAll();
    app->setVisible(true);
    nanogui::mainloop(10);
    run = false;
    // synth_thread.join();
    glfwSetKeyCallback(app->glfwWindow(), NULL);
  }
  nanogui::shutdown();
}
