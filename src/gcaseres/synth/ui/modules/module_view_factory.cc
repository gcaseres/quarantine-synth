#include "module_view_factory.h"

#include "../../modules/envelope.h"
#include "envelope_view.h"
#include "../../modules/vca.h"
#include "vca_view.h"
#include "../../modules/vco.h"
#include "vco_view.h"
#include "../../modules/speaker.h"
#include "speaker_view.h"

using namespace gcaseres::synth::ui::modules;
using namespace gcaseres::synth::modules;

ModuleView* ModuleViewFactory::Create(std::shared_ptr<Module> module,
                                      Widget* parent) {
  if (module->GetModuleType() == Envelope::MODULE_TYPE) {
    return new EnvelopeView(std::static_pointer_cast<Envelope>(module), parent);
  }
  if (module->GetModuleType() == VCA::MODULE_TYPE) {
    return new VCAView(std::static_pointer_cast<VCA>(module), parent);
  }
  if (module->GetModuleType() == VCO::MODULE_TYPE) {
    return new VCOView(std::static_pointer_cast<VCO>(module), parent);
  }
  if (module->GetModuleType() == Speaker::MODULE_TYPE) {
    return new SpeakerView(std::static_pointer_cast<Speaker>(module), parent);
  }

  return nullptr;
}