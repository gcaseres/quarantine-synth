#ifndef GCASERES_SYNTH_UI_MODULES_SPEAKERVIEW_H_
#define GCASERES_SYNTH_UI_MODULES_SPEAKERVIEW_H_

#include "../../modules/speaker.h"
#include "module_view.h"

using gcaseres::synth::modules::Speaker;

namespace gcaseres::synth::ui::modules {

class SpeakerView : public ModuleView {
 public:
  SpeakerView(std::shared_ptr<Speaker> model, Widget* parent);
  virtual void draw(NVGcontext* ctx) override;
};

}  // namespace gcaseres::synth::ui::modules

#endif