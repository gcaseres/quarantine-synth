#ifndef GCASERES_SYNTH_UI_MODULES_MODULEVIEW_H_
#define GCASERES_SYNTH_UI_MODULES_MODULEVIEW_H_

#include <nanogui/widget.h>
#include "../../modules/module.h"
#include "../widgets/container.h"

using namespace nanogui;
using gcaseres::synth::ui::widgets::Container;
using gcaseres::synth::modules::Module;

namespace gcaseres::synth::ui::modules {

class ModuleView : public Widget {
 public:
  ModuleView(std::shared_ptr<Module> model, Widget *parent);
  virtual void draw(NVGcontext *ctx) override;
  virtual bool mouseButtonEvent(const nanogui::Vector2i &p, int button,
                                bool down, int modifiers) override;
  virtual bool mouseDragEvent(const nanogui::Vector2i &,
                              const nanogui::Vector2i &rel, int button,
                              int /* modifiers */) override;
  virtual Vector2i preferredSize(NVGcontext *ctx) const override;
  virtual void performLayout(NVGcontext *ctx) override;
 protected:
  std::string name_;
  std::shared_ptr<Module> model_;
  bool is_dragging_ = false;
  Vector2i preferred_size_;
  Container* children_container_;
};

}  // namespace gcaseres::synth::ui::modules

#endif