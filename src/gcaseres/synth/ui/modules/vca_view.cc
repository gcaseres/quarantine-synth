#include "vca_view.h"
#include "nanogui/slider.h"
#include "nanogui/label.h"
#include "nanogui/layout.h"

using namespace gcaseres::synth::ui::modules;

using gcaseres::synth::modules::VCA;

VCAView::VCAView(std::shared_ptr<VCA> model, Widget* parent)
    : ModuleView(model, parent) {
  this->setSize(Vector2i(200, 80));
  this->name_ = "VCA";

  this->children_container_->setLayout(
      new GridLayout(Orientation::Horizontal, 2, Alignment::Fill));

  new Label(this->children_container_, "Gain: ");
  Slider* gain_slider = new Slider(this->children_container_);
  gain_slider->setValue(model->GetGain());
  gain_slider->setRange(std::pair<float, float>(.0F, 5.F));

  gain_slider->setCallback(
      [model](float value) { model->SetGain(value); });
}

/*
void VCAView::Render() {
  std::shared_ptr<VCA> vca = std::static_pointer_cast<VCA>(this->model_);

  ImGui::Begin("VCA");

  float gain_knob_value = vca->GetGain();
  ImGui::SliderFloat("Gain", &gain_knob_value, 0.0f, 5.0f);
  vca->SetGain(gain_knob_value);

  ImGui::End();
}
*/