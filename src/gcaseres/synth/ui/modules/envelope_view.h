#ifndef GCASERES_SYNTH_UI_MODULES_ENVELOPEVIEW_H_
#define GCASERES_SYNTH_UI_MODULES_ENVELOPEVIEW_H_

#include "../../modules/envelope.h"
#include "module_view.h"

using gcaseres::synth::modules::Envelope;

namespace gcaseres::synth::ui::modules {

class EnvelopeView : public ModuleView {
 public:
  EnvelopeView(std::shared_ptr<Envelope> model, Widget* parent);
};

}  // namespace gcaseres::synth::ui::modules

#endif