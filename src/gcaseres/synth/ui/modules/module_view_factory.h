#ifndef GCASERES_SYNTH_UI_MODULES_MODULEVIEWFACTORY_H_
#define GCASERES_SYNTH_UI_MODULES_MODULEVIEWFACTORY_H_

#include <nanogui/widget.h>
#include "module_view.h"

namespace gcaseres::synth::ui::modules {
class ModuleViewFactory {
 public:
  ModuleView* Create(std::shared_ptr<Module> module,
                                     Widget* parent);
};
}  // namespace gcaseres::synth::ui::modules

#endif