#ifndef GCASERES_SYNTH_UI_MODULES_VCAVIEW_H_
#define GCASERES_SYNTH_UI_MODULES_VCAVIEW_H_

#include "../../modules/vca.h"
#include "module_view.h"

using gcaseres::synth::modules::VCA;

namespace gcaseres::synth::ui::modules {

class VCAView : public ModuleView {
 public:
  VCAView(std::shared_ptr<VCA> model, Widget* parent);
};

}  // namespace gcaseres::synth::ui::modules

#endif