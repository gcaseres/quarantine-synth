#include "speaker_view.h"
#include <nanogui/opengl.h>

using namespace gcaseres::synth::ui::modules;

using gcaseres::synth::modules::Speaker;

SpeakerView::SpeakerView(std::shared_ptr<Speaker> model, Widget* parent)
    : ModuleView(model, parent) {
  this->setSize(Vector2i(200, 100));
  this->name_ = "Speaker";
}

void SpeakerView::draw(NVGcontext* ctx) {
  ModuleView::draw(ctx);

  nvgSave(ctx);

  std::shared_ptr<Speaker> speaker =
      std::static_pointer_cast<Speaker>(this->model_);

  char* signal = speaker->PeekAudioSignalInput();
  if (signal != NULL) {
    nvgBeginPath(ctx);

    Vector2i pos = this->position() + this->children_container_->position();
    Vector2i size = this->children_container_->size();
    nvgMoveTo(ctx, pos.x(), pos.y() + size.y() / 2);

    for (int i = 0; i < size.x(); i++) {
      float t = (float)i / size.x();
      Vector2i point =
          pos + Vector2i(i, signal[(int)(speaker->GetSignalBufferSize() * t)] /
                                128.0 * size.y() / 2 + size.y() / 2);
      nvgLineTo(ctx, point.x(), point.y());
    }
    nvgStrokeColor(ctx, Color(.0F, 0.7F, 0.0F, 1.0F));

    nvgStroke(ctx);
  }

  nvgRestore(ctx);
}
/*
void SpeakerView::Render() {
  std::shared_ptr<Speaker> speaker =
      std::static_pointer_cast<Speaker>(this->model_);

    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
    ImGui::Begin("Speaker");

    ImVec2 window_pos = ImGui::GetWindowPos();
    ImVec2 window_size = ImGui::GetWindowSize();

    char* signal = speaker->PeekAudioSignalInput();
    if (signal != NULL) {
      int width = ImGui::GetWindowContentRegionMax().x -
                  ImGui::GetWindowContentRegionMin().x;
      int height = ImGui::GetWindowContentRegionMax().y -
                   ImGui::GetWindowContentRegionMin().y;
      for (int i = 0; i < width; i++) {
        float t = (float)i / width;

        ImGui::GetWindowDrawList()->AddCircleFilled(
            ImVec2(
                window_pos.x + ImGui::GetWindowContentRegionMin().x + width * t,
                window_pos.y + ImGui::GetWindowContentRegionMin().y + height / 2
    + signal[(int)(speaker->GetSignalBufferSize() * t)] / 128.0 * height / 2.0),
            1, ImColor(1.0, 0.0, 0.0, 1.0F));
      }
    }

    ImGui::End();
    ImGui::PopStyleVar();
}
*/