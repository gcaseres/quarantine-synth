#include "vco_view.h"
#include "nanogui/combobox.h"

using namespace gcaseres::synth::ui::modules;

using gcaseres::synth::modules::VCO;

VCOView::VCOView(std::shared_ptr<VCO> model, Widget* parent)
    : ModuleView(model, parent) {
  this->setSize(Vector2i(200, 80));
  this->name_ = "VCA";

  std::vector<std::string> items = { "Sine", "Square", "Sawtooth" };
  new ComboBox(this->children_container_, items);
}
