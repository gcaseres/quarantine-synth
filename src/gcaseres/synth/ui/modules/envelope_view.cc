
#include <sstream>
#include "envelope_view.h"
#include "nanogui/slider.h"
#include "nanogui/label.h"
#include "nanogui/layout.h"

using namespace gcaseres::synth::ui::modules;
using namespace gcaseres::synth::ui::widgets;

using gcaseres::synth::modules::Envelope;

EnvelopeView::EnvelopeView(std::shared_ptr<Envelope> model, Widget* parent)
    : ModuleView(model, parent) {
  this->setSize(Vector2i(250, 150));
  this->name_ = "Envelope";

  this->children_container_->setLayout(
      new GridLayout(Orientation::Horizontal, 2, Alignment::Fill));

  new Label(this->children_container_, "Attack: ");
  Slider* attack_slider = new Slider(this->children_container_);
  attack_slider->setValue(model->GetAttack() / 50000.0F);
  attack_slider->setRange(std::pair<float, float>(.0F, 1.F));

  new Label(this->children_container_, "Decay: ");
  Slider* decay_slider = new Slider(this->children_container_);
  decay_slider->setValue(model->GetDecay() / 50000.0F);
  decay_slider->setRange(std::pair<float, float>(.0F, 1.F));

  new Label(this->children_container_, "Sustain: ");
  Slider* sustain_slider = new Slider(this->children_container_);
  sustain_slider->setValue(model->GetSustain());
  sustain_slider->setRange(std::pair<float, float>(.0F, 1.F));

  new Label(this->children_container_, "Release: ");
  Slider* release_slider = new Slider(this->children_container_);
  release_slider->setValue(model->GetRelease() / 50000.0F);
  release_slider->setRange(std::pair<float, float>(.0F, 1.F));

  attack_slider->setCallback(
      [model](float value) { model->SetAttack(value * 50000); });

  decay_slider->setCallback(
      [model](float value) { model->SetDecay(value * 50000); });
  sustain_slider->setCallback([model, sustain_slider](float value) {
    std::ostringstream ss;
    ss << value;

    model->SetSustain(value);

    sustain_slider->setTooltip(ss.str());
  });
  release_slider->setCallback(
      [model](float value) { model->SetRelease(value * 50000); });
}
