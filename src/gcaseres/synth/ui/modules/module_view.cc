#include "module_view.h"

#include <nanogui/opengl.h>
#include <nanogui/layout.h>
#include <nanogui/label.h>

using gcaseres::synth::ui::modules::ModuleView;
using gcaseres::synth::ui::widgets::Container;

ModuleView::ModuleView(std::shared_ptr<Module> model, Widget *parent)
    : Widget(parent) {
  this->model_ = model;

  this->children_container_ = new Container(this);
  this->children_container_->setLayout(
      new BoxLayout(Orientation::Vertical, Alignment::Fill));
}

void ModuleView::draw(NVGcontext *ctx) {
  nvgSave(ctx);

  nvgBeginPath(ctx);
  nvgRect(ctx, this->position().x(), this->position().y(), this->size().x(),
          this->size().y());
  nvgStrokeWidth(ctx, 1);
  nvgStrokeColor(ctx, Color(.1F, .1F, .1F, 1.0F));
  nvgFillColor(ctx, Color(.3F, .3F, .3F, 1.0F));
  nvgFill(ctx);
  nvgStroke(ctx);

  nvgBeginPath(ctx);
  nvgRect(ctx, this->position().x() + 1, this->position().y() + 1,
          this->size().x() - 2, this->size().y() - 2);
  nvgStrokeWidth(ctx, 1);
  nvgStrokeColor(ctx, Color(.5F, .5F, .5F, 1.0F));
  nvgStroke(ctx);

  nvgBeginPath(ctx);
  nvgRect(ctx, this->position().x(), this->position().y(), this->size().x(),
          30);
  nvgStrokeWidth(ctx, 1);
  nvgFillColor(ctx, Color(.5F, .5F, .5F, 1.0F));
  nvgFill(ctx);

  nvgFillColor(ctx, Color(1.0F, 1.0F, 1.0F, 1.0F));
  nvgTextAlign(ctx, NVG_ALIGN_MIDDLE | NVG_ALIGN_CENTER);

  nvgText(ctx, this->position().x() + this->size().x() / 2,
          this->position().y() + 30.0 / 2, this->name_.c_str(), NULL);

  nvgRestore(ctx);

  Widget::draw(ctx);
}

Vector2i ModuleView::preferredSize(NVGcontext *ctx) const {
  Vector2i preferred_size = Widget::preferredSize(ctx);
  return this->size();
}

void ModuleView::performLayout(NVGcontext *ctx) {
  this->children_container_->setPosition(Vector2i(10, 40));
  this->children_container_->setSize(this->size() - Vector2i(20, 50));
  this->children_container_->performLayout(ctx);
}

bool ModuleView::mouseButtonEvent(const nanogui::Vector2i &p, int button,
                                  bool down, int modifiers) {
  if (Widget::mouseButtonEvent(p, button, down, modifiers)) return true;
  if (button == GLFW_MOUSE_BUTTON_1) {
    this->is_dragging_ = down && (p.y() - this->position().y());
    return true;
  }
  return false;
}

bool ModuleView::mouseDragEvent(const nanogui::Vector2i &,
                                const nanogui::Vector2i &rel, int button,
                                int /* modifiers */) {
  if (this->is_dragging_ && (button & (1 << GLFW_MOUSE_BUTTON_1)) != 0) {
    this->setPosition(this->position() + rel);
    return true;
  }
  return false;
}