#ifndef GCASERES_SYNTH_UI_MODULES_VCOVIEW_H_
#define GCASERES_SYNTH_UI_MODULES_VCOVIEW_H_

#include "../../modules/vco.h"
#include "module_view.h"

using gcaseres::synth::modules::VCO;

namespace gcaseres::synth::ui::modules {

class VCOView : public ModuleView {
 public:
  VCOView(std::shared_ptr<VCO> model, Widget* parent);
};

}  // namespace gcaseres::synth::ui::modules

#endif