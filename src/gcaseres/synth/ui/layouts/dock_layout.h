#ifndef GCASERES_SYNTH_UI_LAYOUTS_DOCKLAYOUT_H_
#define GCASERES_SYNTH_UI_LAYOUTS_DOCKLAYOUT_H_

#include <nanogui/widget.h>
#include <nanogui/layout.h>

using namespace nanogui;

namespace gcaseres::synth::ui::layouts {

class DockLayout : public Layout {
 public:
  virtual void performLayout(NVGcontext *ctx, Widget *widget) const;

  virtual Vector2i preferredSize(NVGcontext *ctx, const Widget *widget) const;
};
}  // namespace gcaseres::synth::ui::layouts
#endif