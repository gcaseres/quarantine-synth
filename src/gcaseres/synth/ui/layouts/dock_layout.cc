#include "dock_layout.h"

using namespace gcaseres::synth::ui::layouts;

void DockLayout::performLayout(NVGcontext *ctx, Widget *widget) const {
  for (int i = 0; i < widget->childCount(); i++) {
    widget->childAt(i)->setSize(widget->size());
    widget->childAt(i)->setPosition(Vector2i(0, 0));
    widget->childAt(i)->performLayout(ctx);
  }
}

Vector2i DockLayout::preferredSize(NVGcontext *ctx,
                                   const Widget *widget) const {
  return widget->size();
}