#ifndef GCASERES_SYNTH_UI_WIDGETS_LAYOUT_WIDGET_H_
#define GCASERES_SYNTH_UI_WIDGETS_LAYOUT_WIDGET_H_

#include <nanogui/widget.h>

using namespace nanogui;

namespace gcaseres::synth::ui::widgets {

class Container : public Widget {
 public:
  Container(Widget* parent);
};

}  // namespace gcaseres::synth::ui::widgets
#endif