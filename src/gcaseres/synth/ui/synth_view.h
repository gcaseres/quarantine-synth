#ifndef GCASERES_SYNTH_UI_SYNTHVIEW_H_
#define GCASERES_SYNTH_UI_SYNTHVIEW_H_

#include <memory>
#include <vector>
#include <nanogui/widget.h>
#include "../synth.h"
#include "modules/module_view.h"
#include "modules/module_view_factory.h"

using namespace nanogui;

namespace gcaseres::synth::ui {

class SynthView : public Widget {
 public:
  SynthView(std::shared_ptr<Synth> model, Widget* parent);
  ~SynthView();
  virtual void draw(NVGcontext *ctx);

 private:
  std::shared_ptr<Synth> model_;
  modules::ModuleViewFactory module_view_factory_;
  std::vector<std::shared_ptr<modules::ModuleView>> modules_;
};
}  // namespace gcaseres::synth::ui
#endif