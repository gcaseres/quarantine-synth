#include "synth_view.h"
#include <nanogui/opengl.h>

using namespace nanogui;
using namespace gcaseres::synth::ui;
using gcaseres::synth::Synth;

SynthView::SynthView(std::shared_ptr<Synth> model, Widget* parent)
    : Widget(parent), module_view_factory_() {
  this->model_ = model;
  for (int i = 0; i < model->GetModuleCount(); i++) {
    this->module_view_factory_.Create(model->GetModule(i), this);
  }
}

SynthView::~SynthView() { this->modules_.clear(); }

void SynthView::draw(NVGcontext* ctx) {
  Widget::draw(ctx);

  nvgBeginPath(ctx);
  nvgRect(ctx, this->position().x(), this->position().y(), this->size().x(),
          this->size().y());
  nvgStrokeWidth(ctx, 3);
  nvgStroke(ctx);
}