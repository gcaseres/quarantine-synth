#include <cstring>
#include "glfw_input_controller.h"

gcaseres::synth::input::GLFWInputController::GLFWInputController() {
  std::memset(this->pressed_keys_, false, 88);
}
void gcaseres::synth::input::GLFWInputController::GLFWKeyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods) {
  switch (action) {
    case GLFW_PRESS:
      switch (key) {
        case GLFW_KEY_A:
          this->pressed_keys_[39] = this->just_pressed_keys_[39] = true;
          break;
        case GLFW_KEY_S:
          this->pressed_keys_[41] = this->just_pressed_keys_[41] = true;
          break;
        case GLFW_KEY_D:
          this->pressed_keys_[43] = this->just_pressed_keys_[43] = true;
          break;
        case GLFW_KEY_F:
          this->pressed_keys_[44] = this->just_pressed_keys_[44] = true;
          break;
        case GLFW_KEY_G:
          this->pressed_keys_[46] = this->just_pressed_keys_[46] = true;
          break;
      }
      break;
    case GLFW_RELEASE:
      switch (key) {
        case GLFW_KEY_A:
          this->pressed_keys_[39] = false;
          break;
        case GLFW_KEY_S:
          this->pressed_keys_[41] = false;
          break;
        case GLFW_KEY_D:
          this->pressed_keys_[43] = false;
          break;
        case GLFW_KEY_F:
          this->pressed_keys_[44] = false;
          break;
        case GLFW_KEY_G:
          this->pressed_keys_[46] = false;
          break;
      }
      break;
  }
}
bool gcaseres::synth::input::GLFWInputController::IsPressed(
    unsigned int key_id) {
  return this->pressed_keys_[key_id];
}
bool gcaseres::synth::input::GLFWInputController::JustPressed(
    unsigned int key_id) {
  return just_pressed_keys_[key_id];
}
void gcaseres::synth::input::GLFWInputController::Update() {
  std::memset(this->just_pressed_keys_, false, 88);
}