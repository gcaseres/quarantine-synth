#ifndef GCASERES_SYNTH_INPUT_GLFWINPUTCONTROLLER_H_
#define GCASERES_SYNTH_INPUT_GLFWINPUTCONTROLLER_H_

#include <GLFW/glfw3.h>
#include "input_controller.h"

namespace gcaseres::synth::input {
class GLFWInputController : public InputController {
 public:
  GLFWInputController();
  void GLFWKeyCallback(GLFWwindow *window, int key, int scancode, int action,
                       int mods);
  virtual bool IsPressed(unsigned int key_id);
  virtual bool JustPressed(unsigned int key_id);
  void Update();

 private:
  bool pressed_keys_[88];
  bool just_pressed_keys_[88];
};
}  // namespace gcaseres::synth::input

#endif