#ifndef GCASERES_SYNTH_INPUT_INPUTCONTROLLER_H_
#define GCASERES_SYNTH_INPUT_INPUTCONTROLLER_H_

#include <X11/Xlib.h>

namespace gcaseres::synth::input {
class InputController {
 public:
  virtual bool IsPressed(unsigned int key_id) = 0;
  virtual bool JustPressed(unsigned int key_id) = 0;
};
}  // namespace gcaseres::synth::input

#endif