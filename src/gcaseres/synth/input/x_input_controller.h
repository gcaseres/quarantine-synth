#ifndef GCASERES_SYNTH_INPUT_XINPUTCONTROLLER_H_
#define GCASERES_SYNTH_INPUT_XINPUTCONTROLLER_H_

#include <X11/Xlib.h>
#include "input_controller.h"

namespace gcaseres::synth::input {
class XInputController : public InputController {
 public:
  XInputController(Display* display);
  void ProcessEvent(XEvent event);
  virtual bool IsPressed(unsigned int key_id);
  virtual bool JustPressed(unsigned int key_id);
  void Update();
  
 private:
  Display* display_;
  bool pressed_keys_[88];
  bool just_pressed_keys_[88];
};
}  // namespace gcaseres::synth::input

#endif