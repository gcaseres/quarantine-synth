#include "x_input_controller.h"

#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <string.h>

#include <iostream>

gcaseres::synth::input::XInputController::XInputController(Display* display) {
  this->display_ = display;
  memset(this->pressed_keys_, false, 88);
}

void gcaseres::synth::input::XInputController::Update() {
  memset(this->just_pressed_keys_, false, 88);
}

void gcaseres::synth::input::XInputController::ProcessEvent(XEvent event) {
  // std::wcout << "EVENT: " << event.type << std::endl;
  switch (event.type) {
    case KeyPress:
      switch (XkbKeycodeToKeysym(this->display_, event.xkey.keycode, 0, 0)) {
        case XK_A | XK_a:
          this->pressed_keys_[39] = true;
          this->just_pressed_keys_[39] = true;
          break;
        case XK_S | XK_s:
          this->pressed_keys_[41] = true;
          this->just_pressed_keys_[41] = true;
          break;
        case XK_D | XK_d:
          this->pressed_keys_[43] = true;
          this->just_pressed_keys_[43] = true;
          break;
        case XK_F | XK_f:
          this->pressed_keys_[44] = true;
          this->just_pressed_keys_[44] = true;
          break;
        case XK_G | XK_g:
          this->pressed_keys_[46] = true;
          this->just_pressed_keys_[46] = true;
          break;
        default:
          break;
      }
      break;
    case KeyRelease:
      if (XEventsQueued(this->display_, QueuedAfterReading)) {
        XEvent next_event;
        XPeekEvent(this->display_, &next_event);
        if (next_event.type == KeyPress &&
            next_event.xkey.time == event.xkey.time &&
            next_event.xkey.keycode == event.xkey.keycode) {
          XNextEvent(this->display_, &next_event);
          break;
        }
      }
      switch (XkbKeycodeToKeysym(this->display_, event.xkey.keycode, 0, 0)) {
        case XK_A | XK_a:
          this->pressed_keys_[39] = false;
          break;
        case XK_S | XK_s:
          this->pressed_keys_[41] = false;
          break;
        case XK_D | XK_d:
          this->pressed_keys_[43] = false;
          break;
        case XK_F | XK_f:
          this->pressed_keys_[44] = false;
          break;
        case XK_G | XK_g:
          this->pressed_keys_[46] = false;
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}

bool gcaseres::synth::input::XInputController::IsPressed(unsigned int key_id) {
  return this->pressed_keys_[key_id];
}

bool gcaseres::synth::input::XInputController::JustPressed(
    unsigned int key_id) {
  return this->just_pressed_keys_[key_id];
}
