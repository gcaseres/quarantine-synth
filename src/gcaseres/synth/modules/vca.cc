#include "vca.h"

#include <math.h>

using namespace gcaseres::synth::modules;

const ModuleType VCA::MODULE_TYPE = "GCASERES::SYNTH::VCA";

VCA::VCA(unsigned int audio_signal_buffer_size) : modules::Module() {
  this->gain_ = 1.0;
  this->inputs_.push_back(std::make_unique<Input>(sizeof(char) * audio_signal_buffer_size));
  this->inputs_.push_back(std::make_unique<Input>(sizeof(double) * audio_signal_buffer_size));
  this->outputs_.push_back(std::make_unique<Output>());
  this->audio_signal_buffer_size_ = audio_signal_buffer_size;
}

void VCA::Process() {
  char* audio_signal = this->GetAudioSignalInput();
  double* amplitude_signal = this->GetAmplitudeSignalInput();

  for (int i = 0; i < this->audio_signal_buffer_size_; i++) {
    audio_signal[i] = (char)std::max(
        -128.0,
        std::min(127.0, audio_signal[i] * amplitude_signal[i] * this->gain_));
  }

  this->SendAudioSignal(audio_signal);
}

ModuleType VCA::GetModuleType() { return VCA::MODULE_TYPE; }

char* VCA::ReceiveAudioSignal(char* signal) {
  this->inputs_.at(0)->SetSignal(&signal);
}

void VCA::ReceiveAmplitudeSignal(double* amplitude) {
  this->inputs_.at(1)->SetSignal(&amplitude);
}

void VCA::SendAudioSignal(char* signal) {
  this->outputs_.at(0)->SendSignal(signal);
}

char* VCA::GetAudioSignalInput() {
  return (char*)this->inputs_.at(0)->GetSignal();
}
double* VCA::GetAmplitudeSignalInput() {
  return (double*)this->inputs_.at(1)->GetSignal();
}

void VCA::SetGain(float gain) { this->gain_ = gain; }

float VCA::GetGain() { return this->gain_; }
