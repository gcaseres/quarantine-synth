#ifndef GCASERES_OUTPUT_H_
#define GCASERES_OUTPUT_H_

#include <memory>
#include "module.h"

namespace gcaseres::synth::modules {

class Module;

class Output {
 public:
  Output();
  void Connect(std::weak_ptr<Module> module, unsigned int input_index);
  void SendSignal(void* signal);

 private:
  std::weak_ptr<Module> connected_module_;
  int connected_module_input_index_;
  void* signal_;
};

}  // namespace gcaseres::synth::modules

#endif