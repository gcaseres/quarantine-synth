#ifndef GCASERES_CV_GATE_KEYBOARD_H_
#define GCASERES_CV_GATE_KEYBOARD_H_

#include "../input/input_controller.h"
#include "../signals.h"
#include "module.h"

namespace gcaseres::synth::modules {

class CvGateKeyboard : public Module {
 public:
  static const ModuleType MODULE_TYPE;
  CvGateKeyboard(gcaseres::synth::input::InputController& input_controller,
                 unsigned int control_signal_buffer_size);
  virtual void Process();
  virtual ModuleType GetModuleType();

 private:
  void SendControlSignal(gcaseres::synth::signal* signal);
  void SendGateSignal(bool signal);
  void SendTriggerSignal(bool signal);
  float GetControlSignalFromFrequency(float frequency);
  gcaseres::synth::input::InputController* input_controller_;
  float* control_signal_;
  unsigned int control_signal_buffer_size_;
  Display* display_;
  bool pressed_keys_[88];
  unsigned int keys_frequencies_[88] = {
      0,   0,   0,                                                 // 0
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    // 1
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    // 2
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    // 3
      261, 277, 293, 311, 329, 349, 369, 392, 415, 440, 466, 493,  // 4
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    // 5
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    // 6
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,    // 7
      0                                                            // 8
  };
};
}  // namespace gcaseres::synth::modules

#endif