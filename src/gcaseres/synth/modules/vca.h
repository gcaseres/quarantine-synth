#ifndef GCASERES_SYNTH_MODULES_VCA_H_
#define GCASERES_SYNTH_MODULES_VCA_H_

#include "module.h"

namespace gcaseres::synth::modules {

class VCA : public Module {
 public:
  static const ModuleType MODULE_TYPE;
  VCA(unsigned int audio_signal_buffer_size);
  virtual void Process();
  virtual ModuleType GetModuleType();
  void ReceiveAmplitudeSignal(double* amplitude);
  char* ReceiveAudioSignal(char* signal);
  void SetGain(float gain);
  float GetGain();

 private:
  void SendAudioSignal(char* signal);
  char* GetAudioSignalInput();
  double* GetAmplitudeSignalInput();
  unsigned int audio_signal_buffer_size_;
  float gain_;
};
}  // namespace gcaseres::synth::modules

#endif