#ifndef GCASERES_SPEAKER_H_
#define GCASERES_SPEAKER_H_

#include <alsa/asoundlib.h>

#include "module.h"

namespace gcaseres::synth::modules {

class Speaker : public Module {
 public:
  static const ModuleType MODULE_TYPE;
  Speaker(unsigned int signal_buffer_size, snd_pcm_t* pcm);
  virtual void Process();
  virtual ModuleType GetModuleType();  
  char* PeekAudioSignalInput();
  unsigned int GetSignalBufferSize();
  
 private:
  char* GetAudioSignalInput();
  snd_pcm_t* pcm_;
  unsigned int signal_buffer_size_;
};
}  // namespace gcaseres::synth::modules

#endif