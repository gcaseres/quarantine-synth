#include "speaker.h"

using namespace gcaseres::synth::modules;

const ModuleType Speaker::MODULE_TYPE = "GCASERES::SYNTH::SPEAKER";

Speaker::Speaker(unsigned int signal_buffer_size, snd_pcm_t* pcm)
    : Module::Module() {
  this->inputs_.push_back(
      std::make_unique<Input>(sizeof(unsigned int) * signal_buffer_size));
  this->pcm_ = pcm;
  this->signal_buffer_size_ = signal_buffer_size;
}

void Speaker::Process() {
  char* pcm_playback_buffer = this->GetAudioSignalInput();

  while (snd_pcm_writei(this->pcm_, pcm_playback_buffer,
                        this->signal_buffer_size_) < 0) {
    snd_pcm_prepare(this->pcm_);
  }
}

ModuleType Speaker::GetModuleType() { return Speaker::MODULE_TYPE; }

char* Speaker::GetAudioSignalInput() {
  return (char*)this->inputs_.at(0)->GetSignal();
}

char* Speaker::PeekAudioSignalInput() {
  return (char*)this->inputs_.at(0)->PeekSignal();
}

unsigned int Speaker::GetSignalBufferSize() {
  return this->signal_buffer_size_;
}
