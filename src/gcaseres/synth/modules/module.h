#ifndef GCASERES_MODULE_H_
#define GCASERES_MODULE_H_

#include <vector>
#include <memory>
#include "input.h"
#include "output.h"

namespace gcaseres::synth::modules {

class Input;
class Output;

typedef std::string ModuleType;

class Module : public std::enable_shared_from_this<Module> {
 public:
  Module();
  ~Module();
  virtual void Process() = 0;
  virtual ModuleType GetModuleType() = 0;
  virtual void ReceiveSignal(int input_index, void* signal);
  void Connect(unsigned int output_index, std::weak_ptr<Module> module,
               unsigned int input_index);

 protected:
  std::vector<std::unique_ptr<Input>> inputs_;
  std::vector<std::unique_ptr<Output>> outputs_;
};
}  // namespace gcaseres::synth::modules

#endif