#include "vco.h"
#include <cstring>
#include <cmath>

using namespace gcaseres::synth::modules;

const ModuleType VCO::MODULE_TYPE = "GCASERES::SYNTH::VCO";

VCO::VCO(unsigned int sample_rate, unsigned int output_signal_buffer_size)
    : Module::Module() {
  this->sample_rate_ = sample_rate;
  this->output_signal_buffer_size_ = output_signal_buffer_size;
  this->output_signal_buffer_ = (char*)malloc(output_signal_buffer_size);
  this->frame_ = 0;
  this->inputs_.push_back(std::make_unique<Input>(sizeof(float) * output_signal_buffer_size));
  this->outputs_.push_back(std::make_unique<Output>());
}

void VCO::Process() {
  float* control_signal = this->GetControlSignalInput();

  for (int i = 0; i < this->output_signal_buffer_size_; i++) {
    float frequency = this->ReadFrequencyFromControlSignal(control_signal[i]);

    this->output_signal_buffer_[i] =
        sin(frequency * 2.0 * M_PI * (double)(this->frame_ + i) /
            this->sample_rate_) *
        127;
  }

  this->frame_ += this->output_signal_buffer_size_;
  this->frame_ = this->frame_ % this->sample_rate_;

  this->SendAudioSignal(this->output_signal_buffer_);
}

ModuleType VCO::GetModuleType() { return VCO::MODULE_TYPE; }

float VCO::ReadFrequencyFromControlSignal(gcaseres::synth::signal signal) {
  float MIN_FREQUENCY = 1.021975;

  float unsigned_signal = signal + gcaseres::synth::kSignalMaxAmplitude;
  float fq = MIN_FREQUENCY;

  for (int i = 0; i < (int)unsigned_signal; i++) {
    fq *= 2;
  }

  float signal_decimal_part = unsigned_signal - (int)unsigned_signal;

  return (1.0 - signal_decimal_part) * fq + signal_decimal_part * fq * 2;
}

void VCO::SendAudioSignal(char* signal) {
  this->outputs_.at(0)->SendSignal(signal);
}

void VCO::ReceiveControlSignal(gcaseres::synth::signal* signal) {
  this->ReceiveSignal(0, signal);
}

void VCO::SetWaveShape(VCO::WaveShape wave_shape) {
  this->wave_shape_ = wave_shape;
}

VCO::WaveShape VCO::GetWaveShape() {
  return this->wave_shape_;
}

gcaseres::synth::signal* VCO::GetControlSignalInput() {
  return (float*)this->inputs_.at(0)->GetSignal();
}
