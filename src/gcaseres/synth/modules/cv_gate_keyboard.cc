#include "cv_gate_keyboard.h"
#include <cstdlib>
#include "vco.h"
#include <math.h>

using namespace gcaseres::synth::modules;

const ModuleType CvGateKeyboard::MODULE_TYPE =
    "GCASERES::SYNTH::CV_GATE_KEYBOARD";

CvGateKeyboard::CvGateKeyboard(
    gcaseres::synth::input::InputController& input_controller,
    unsigned int control_signal_buffer_size)
    : Module() {
  this->control_signal_ =
      (float*)malloc(sizeof(float) * control_signal_buffer_size);
  this->control_signal_buffer_size_ = control_signal_buffer_size;
  this->outputs_.push_back(std::make_unique<Output>());
  this->outputs_.push_back(std::make_unique<Output>());
  this->outputs_.push_back(std::make_unique<Output>());
  this->input_controller_ = &input_controller;
}

void CvGateKeyboard::Process() {
  // TODO: Mejorar mecanismo para mantener el orden de tocada
  bool trigger = false;
  bool any_pressed_key = false;
  float frequency = 0;
  for (int key_id = 0; key_id < 88; key_id++) {
    if (this->input_controller_->IsPressed(key_id)) {
      any_pressed_key = true;
      frequency = this->keys_frequencies_[key_id];
      if (this->input_controller_->JustPressed(key_id)) {
        trigger = true;
        break;
      }
    }
  }

  if (any_pressed_key) {
    gcaseres::synth::signal control_signal =
        this->GetControlSignalFromFrequency(frequency);
    for (int i = 0; i < this->control_signal_buffer_size_; i++) {
      this->control_signal_[i] = control_signal;
    }
  }

  this->SendTriggerSignal(trigger);
  this->SendGateSignal(any_pressed_key);
  this->SendControlSignal(this->control_signal_);
}

ModuleType CvGateKeyboard::GetModuleType() {
  return CvGateKeyboard::MODULE_TYPE;
}

gcaseres::synth::signal CvGateKeyboard::GetControlSignalFromFrequency(
    float frequency) {
  const float kMinFrequency =
      VCO::kZeroVoltFrequency / pow(2, gcaseres::synth::kSignalMaxAmplitude);
  unsigned short oct;

  while (frequency / 2 > kMinFrequency) {
    frequency /= 2;
    oct++;
  }

  return frequency / kMinFrequency - gcaseres::synth::kSignalMaxAmplitude +
         oct - 1;
}

void CvGateKeyboard::SendControlSignal(gcaseres::synth::signal* signal) {
  this->outputs_.at(0)->SendSignal(signal);
}

void CvGateKeyboard::SendGateSignal(bool signal) {
  this->outputs_.at(1)->SendSignal(&signal);
}

void CvGateKeyboard::SendTriggerSignal(bool signal) {
  this->outputs_.at(2)->SendSignal(&signal);
}
