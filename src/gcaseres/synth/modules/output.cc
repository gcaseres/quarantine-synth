#include "output.h"

using namespace gcaseres::synth::modules;

Output::Output() {

}

void Output::Connect(std::weak_ptr<Module> module, unsigned int input_index) {
  this->connected_module_ = module;
  this->connected_module_input_index_ = input_index;
}

void Output::SendSignal(void* signal) {
  if (this->connected_module_.lock()) {
    this->connected_module_.lock()->ReceiveSignal(this->connected_module_input_index_,
                                           signal);
  }
}
