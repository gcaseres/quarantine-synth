#ifndef GCASERES_INPUT_H_
#define GCASERES_INPUT_H_

#include <memory>
#include "module.h"

namespace gcaseres::synth::modules {

class Module;

class Input {
 public:
  Input(unsigned int size);
  ~Input();
  void Connect(std::weak_ptr<Module> module, unsigned int output_index);
  void SetSignal(void* signal);
  void* GetSignal();
  void* PeekSignal();

 protected:
 private:
  unsigned int size_;
  bool has_signal_;
  void* signal_;
  std::weak_ptr<Module> connected_module_;
  unsigned int connected_module_output_index_;
};

}  // namespace gcaseres::synth::modules

#endif