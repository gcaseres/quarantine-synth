#ifndef GCASERES_VCO_H_
#define GCASERES_VCO_H_

#include "module.h"
#include "../signals.h"

namespace gcaseres::synth::modules {

class VCO : public Module {
 public:
  enum WaveShape { Sine, Square, Sawtooth };
  static const ModuleType MODULE_TYPE;
  VCO(unsigned int sample_rate, unsigned int output_signal_buffer_size);
  virtual void Process();
  virtual ModuleType GetModuleType();
  void ReceiveControlSignal(float* frequency);
  void SetWaveShape(WaveShape wave_shape);
  WaveShape GetWaveShape();
  static constexpr const float kZeroVoltFrequency = 261.6256;

 private:
  void SendAudioSignal(char* signal);
  signal* GetControlSignalInput();
  float ReadFrequencyFromControlSignal(signal signal);
  unsigned int frame_;
  unsigned int sample_rate_;
  char* output_signal_buffer_;
  unsigned int output_signal_buffer_size_;
  WaveShape wave_shape_;
};
}  // namespace gcaseres::synth::modules

#endif