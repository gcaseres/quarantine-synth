#include "envelope.h"

#include <stdlib.h>
#include <string.h>

#include <iostream>

using namespace gcaseres::synth::modules;

const ModuleType Envelope::MODULE_TYPE = "GCASERES::SYNTH::ENVELOPE";

Envelope::Envelope(unsigned int amplitude_signal_buffer_size)
    : Module::Module() {
  this->inputs_.push_back(std::make_unique<Input>(sizeof(bool)));
  this->inputs_.push_back(std::make_unique<Input>(sizeof(bool)));
  this->outputs_.push_back(std::make_unique<Output>());
  this->amplitude_signal_buffer_size_ = amplitude_signal_buffer_size;
  this->amplitude_signal_buffer_ =
      (double*)malloc(sizeof(double) * amplitude_signal_buffer_size);
  this->releasing_ = false;
  this->active_ = false;
  this->last_amplitude_ = 0;
}

Envelope::~Envelope() { free(this->amplitude_signal_buffer_); }

void Envelope::ReceiveTriggerSignal(bool signal) {
  this->inputs_.at(0)->SetSignal(&signal);
}

void Envelope::ReceiveGateSignal(bool signal) {
  this->inputs_.at(1)->SetSignal(&signal);
}

void Envelope::SetAttack(unsigned int attack) { this->attack_ = attack; }

unsigned int Envelope::GetAttack() { return this->attack_; }

void Envelope::SetDecay(unsigned int decay) { this->decay_ = decay; }

unsigned int Envelope::GetDecay() { return this->decay_; }

void Envelope::SetSustain(double sustain) { this->sustain_ = sustain; }

double Envelope::GetSustain() { return this->sustain_; }

void Envelope::SetRelease(unsigned int release) { this->release_ = release; }

unsigned int Envelope::GetRelease() { return this->release_; }

void Envelope::Process() {
  bool trigger = this->GetTriggerSignal();
  bool gate = this->GetGateSignal();

  if (trigger) {
    this->frames_since_trigger_ = 0;
    this->releasing_ = false;
    this->active_ = true;
    this->attack_end_frame_ = this->frames_since_trigger_ + this->attack_;
    this->decay_end_frame_ = this->attack_end_frame_ + this->decay_;
    this->attack_start_amplitude_ = this->last_amplitude_;
  }

  if (!this->active_) {
    for (int i = 0; i < this->amplitude_signal_buffer_size_; i++) {
      this->amplitude_signal_buffer_[i] = 0.0;
    }
    this->last_amplitude_ = 0;
  } else {
    if (!gate) {
      if (!this->releasing_) {
        this->releasing_ = true;
        this->release_frame = this->frames_since_trigger_;
        this->release_end_frame_ = this->frames_since_trigger_ + this->release_;
        this->release_start_amplitude_ = this->last_amplitude_;
      }
    }

    for (int i = 0; i < this->amplitude_signal_buffer_size_; i++) {
      if (!this->active_) {
        this->amplitude_signal_buffer_[i] = 0;
      } else {
        if (this->releasing_) {
          if (this->frames_since_trigger_ > this->release_end_frame_) {
            this->active_ = false;
            this->amplitude_signal_buffer_[i] = 0;
          } else {
            // Release stage
            unsigned int frames_since_release =
                this->frames_since_trigger_ - this->release_frame;
            this->amplitude_signal_buffer_[i] =
                (1 - (double)frames_since_release / this->release_) *
                this->release_start_amplitude_;
          }
        } else {
          if (this->frames_since_trigger_ <= this->attack_end_frame_) {
            // Attack stage
            this->amplitude_signal_buffer_[i] =
                (1 - (double)this->frames_since_trigger_ /
                         this->attack_end_frame_) *
                    this->attack_start_amplitude_ +
                (double)this->frames_since_trigger_ / this->attack_end_frame_;
          }

          if (this->frames_since_trigger_ > this->attack_end_frame_ &&
              this->frames_since_trigger_ <= this->decay_end_frame_) {
            // Decay stage
            int frames_since_attack =
                this->frames_since_trigger_ - this->attack_end_frame_;

            this->amplitude_signal_buffer_[i] =
                (1 - (double)frames_since_attack / this->decay_) +
                (double)frames_since_attack / this->decay_ * this->sustain_;
          }

          if (this->frames_since_trigger_ > this->decay_end_frame_) {
            // Sustain stage
            this->amplitude_signal_buffer_[i] = this->sustain_;
          }
        }
      }
      this->last_amplitude_ = this->amplitude_signal_buffer_[i];
      this->frames_since_trigger_ += 1;
    }
  }

  this->SendAmplitudeSignal(this->amplitude_signal_buffer_);
}

ModuleType Envelope::GetModuleType() { return Envelope::MODULE_TYPE; }

void Envelope::SendAmplitudeSignal(double* amplitude) {
  this->outputs_.at(0)->SendSignal(amplitude);
}

bool Envelope::GetTriggerSignal() {
  return *(bool*)this->inputs_.at(0)->GetSignal();
}

bool Envelope::GetGateSignal() {
  return *(bool*)this->inputs_.at(1)->GetSignal();
}