#include "input.h"

#include <cstring>

using namespace gcaseres::synth::modules;

Input::Input(unsigned int size) {
  this->has_signal_ = false;
  this->size_ = size;
  this->signal_ = malloc(size);
}

Input::~Input() { free(this->signal_); }

void Input::SetSignal(void* signal) {
  std::memcpy(this->signal_, signal, this->size_);
  this->has_signal_ = true;
}

void* Input::PeekSignal() {
  return this->signal_;
}

void* Input::GetSignal() {
  if (!this->has_signal_ && this->connected_module_.lock()) {
    this->connected_module_.lock()->Process();
  }

  if (this->has_signal_) {
    this->has_signal_ = false;
    return this->signal_;
  } else {
    return NULL;
  }
}

void Input::Connect(std::weak_ptr<Module> module, unsigned int output_index) {
  this->connected_module_ = module;
  this->connected_module_output_index_ = output_index;
}
