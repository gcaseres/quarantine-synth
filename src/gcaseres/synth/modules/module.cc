#include "module.h"

using namespace gcaseres::synth::modules; 

Module::Module() {
}

Module::~Module() {
}

void Module::ReceiveSignal(int input_index, void* signal) {
    this->inputs_.at(input_index)->SetSignal(signal);
}

void Module::Connect(unsigned int output_index, std::weak_ptr<Module> module, unsigned int input_index) {
    this->outputs_.at(output_index)->Connect(module, input_index);
    module.lock()->inputs_.at(input_index)->Connect(this->shared_from_this(), output_index);
}