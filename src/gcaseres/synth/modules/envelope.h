#ifndef GCASERES_SYNTH_MODULES_ENVELOPE_H_
#define GCASERES_SYNTH_MODULES_ENVELOPE_H_

#include "module.h"

namespace gcaseres::synth::modules {

class Envelope : public Module {
 public:
  static const ModuleType MODULE_TYPE;
  Envelope(unsigned int amplitude_signal_buffer_size);
  ~Envelope();
  virtual void Process();
  virtual ModuleType GetModuleType();
  void ReceiveTriggerSignal(bool signal);
  void ReceiveGateSignal(bool signal);
  void SetAttack(unsigned int attack);
  unsigned int GetAttack();
  void SetDecay(unsigned int decay);
  unsigned int GetDecay();
  void SetSustain(double sustain);
  double GetSustain();
  void SetRelease(unsigned int release);
  unsigned int GetRelease();

 private:
  double* amplitude_signal_buffer_;
  unsigned int amplitude_signal_buffer_size_;
  void SendAmplitudeSignal(double* amplitude);
  bool GetTriggerSignal();
  bool GetGateSignal();
  unsigned int attack_;
  unsigned int decay_;
  double sustain_;
  unsigned int release_;
  unsigned int frames_since_trigger_;
  unsigned int attack_end_frame_;
  double attack_start_amplitude_;
  unsigned int decay_end_frame_;
  unsigned int release_end_frame_;
  unsigned int release_frame;
  double release_start_amplitude_;
  double last_amplitude_;
  bool releasing_;
  bool active_;
};
}  // namespace gcaseres::synth::modules

#endif