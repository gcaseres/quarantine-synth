#ifndef GCASERES_SYNTH_SIGNALS_H_
#define GCASERES_SYNTH_SIGNALS_H_

namespace gcaseres::synth {

const float kSignalMaxAmplitude = 8.0;

typedef float signal;

}  // namespace gcaseres::synth
#endif