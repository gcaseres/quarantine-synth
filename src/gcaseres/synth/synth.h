#ifndef GCASERES_SYNTH_H_
#define GCASERES_SYNTH_H_

#include <memory>
#include <vector>
#include <alsa/asoundlib.h>
#include "modules/module.h"
#include "input/input_controller.h"

using gcaseres::synth::modules::Module;

namespace gcaseres::synth {
class Synth {
 public:
  ~Synth();
  int Initialize(input::InputController& input_controller);
  void Update();
  unsigned short GetModuleCount() const;
  std::shared_ptr<Module> GetModule(int module_index) const;
  std::shared_ptr<Module> output_module_ = NULL;

 private:
  std::vector<std::shared_ptr<Module>> modules_;
  int pcm_channels_ = 1;
  unsigned int pcm_periods_ = 6;
  unsigned int pcm_rate_ = 44100;
  unsigned int pcm_exact_rate;
  snd_pcm_uframes_t pcm_period_frames_ = 1024;
  snd_pcm_t* pcm_;
};
};  // namespace gcaseres::synth
#endif