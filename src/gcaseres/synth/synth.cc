#include "synth.h"

#include <iostream>
#include <limits>
#include <sstream>

#include "modules/cv_gate_keyboard.h"
#include "modules/envelope.h"
#include "modules/speaker.h"
#include "modules/vca.h"
#include "modules/vco.h"

using namespace gcaseres::synth;

Synth::~Synth() {
  if (this->pcm_) {
    // snd_pcm_close(this->pcm_); TODO: Meter en método Finalize
    snd_pcm_hw_free(this->pcm_);
  }
}

int Synth::Initialize(input::InputController& input_controller) {
  int err;
  err = snd_pcm_open(&this->pcm_, "plughw:0,0", SND_PCM_STREAM_PLAYBACK, 0);
  if (err < 0) {
    return err;
  }

  snd_pcm_hw_params_t* pcm_hw_params;

  err = snd_pcm_hw_params_malloc(&pcm_hw_params);
  if (err < 0) {
    return err;
  }

  err = snd_pcm_hw_params_any(this->pcm_, pcm_hw_params);
  if (err < 0) {
    return err;
  }

  err = snd_pcm_hw_params_set_access(this->pcm_, pcm_hw_params,
                                     SND_PCM_ACCESS_RW_INTERLEAVED);
  if (err < 0) {
    return err;
  }

  this->pcm_exact_rate = this->pcm_rate_;
  err = snd_pcm_hw_params_set_rate_near(this->pcm_, pcm_hw_params,
                                        &this->pcm_exact_rate, 0);
  if (err < 0) {
    return err;
  }

  err = snd_pcm_hw_params_set_format(this->pcm_, pcm_hw_params,
                                     SND_PCM_FORMAT_S8);
  if (err < 0) {
    return err;
  }

  err = snd_pcm_hw_params_set_channels(this->pcm_, pcm_hw_params,
                                       this->pcm_channels_);
  if (err < 0) {
    return err;
  }

  err = snd_pcm_hw_params_set_periods(this->pcm_, pcm_hw_params,
                                      this->pcm_periods_, 0);
  if (err < 0) {
    return err;
  }

  err = snd_pcm_hw_params_set_period_size(this->pcm_, pcm_hw_params,
                                          this->pcm_period_frames_, 0);
  if (err < 0) {
    return err;
  }

  snd_pcm_uframes_t buffer_size;
  err = snd_pcm_hw_params_get_buffer_size(pcm_hw_params, &buffer_size);
  if (err < 0) {
    return err;
  }
  std::wcout << buffer_size << std::endl;

  err = snd_pcm_hw_params(this->pcm_, pcm_hw_params);
  if (err < 0) {
    return err;
  }

  snd_pcm_hw_params_free(pcm_hw_params);

  err = snd_pcm_prepare(this->pcm_);
  if (err < 0) {
    return err;
  }

  std::shared_ptr<modules::CvGateKeyboard> cvGateKeyboard(
      new modules::CvGateKeyboard(
          input_controller, this->pcm_period_frames_ * this->pcm_channels_));

  std::shared_ptr<modules::VCO> vco(new modules::VCO(
      this->pcm_exact_rate, this->pcm_period_frames_ * this->pcm_channels_));

  std::shared_ptr<modules::VCA> vca(
      new modules::VCA(this->pcm_period_frames_ * this->pcm_channels_));

  std::shared_ptr<modules::Speaker> speaker(new modules::Speaker(
      this->pcm_period_frames_ * this->pcm_channels_, this->pcm_));

  std::shared_ptr<modules::Envelope> envelope(
      new modules::Envelope(this->pcm_period_frames_ * this->pcm_channels_));

  this->output_module_ = speaker;

  this->modules_.push_back(cvGateKeyboard);
  this->modules_.push_back(vco);
  this->modules_.push_back(vca);
  this->modules_.push_back(speaker);
  this->modules_.push_back(envelope);

  cvGateKeyboard->Connect(0, vco, 0);
  cvGateKeyboard->Connect(1, envelope, 1);  // Gate
  cvGateKeyboard->Connect(2, envelope, 0);  // Trigger

  envelope->Connect(0, vca, 1);
  envelope->SetAttack(50000);
  envelope->SetDecay(10000);
  envelope->SetSustain(0.4);
  envelope->SetRelease(15500);

  vco->Connect(0, vca, 0);

  vca->Connect(0, speaker, 0);

  return 0;
}

unsigned short Synth::GetModuleCount() const { return this->modules_.size(); }

std::shared_ptr<Module> Synth::GetModule(int module_index) const {
  return this->modules_[module_index];
}

void gcaseres::synth::Synth::Update() { this->output_module_->Process(); }
