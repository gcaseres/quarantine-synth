# Quarantine Synth

## Build

### Build and install glfw
```
    cmake .
    make
    make install
```

## Debug

### X11 in docker

In host give permissions
```
xhost local:root
```

### Install NVIDIA drivers
WARNING: Doesn't work
```
sh NVIDIA-Linux-x86_64-410.104.run -a -N --ui=none --no-kernel-module
```
#### References

 - https://gernotklingler.com/blog/howto-get-hardware-accelerated-opengl-support-docker/