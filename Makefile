SOURCEDIR=src
CC=gcc
BINDIR=bin
BUILDDIR=build
OBJDIR=$(BUILDDIR)/obj
LIBDIR=lib

LDFLAGS=-lstdc++ -lm -lasound -lX11 -lglfw3 -lrt -lm -ldl -lpthread -lxcb -lXau -lXdmcp -lnanogui 

OBJS=$(patsubst %.cc,%.o,$(patsubst $(SOURCEDIR)%,$(OBJDIR)%,$(shell find $(SOURCEDIR) -type f -name '*.cc')))

.PHONY: all clean

all: $(BINDIR)/libnanogui.so $(BINDIR)/synth

clean:
	rm -Rf $(OBJDIR) $(BINDIR) $(BUILDDIR)

$(BINDIR)/synth: $(OBJS) $(OBJDIR)/nanogui/nanogui_resources.o | $(BINDIR)/
	$(CC) -g -o $@ $^ -L$(BUILDDIR)/nanogui $(LDFLAGS) 

$(OBJDIR)/gcaseres/%.o: $(SOURCEDIR)/gcaseres/%.cc
	mkdir -p $(dir $@)
	$(CC) -g -c -I lib -I lib/nanogui/include -I lib/nanogui/ext/eigen -I lib/nanogui/ext/glfw/include -I lib/nanogui/ext/nanovg/src -o $@ $(patsubst %.o,%.cc,$(patsubst $(OBJDIR)%,$(SOURCEDIR)%,$@))

$(BINDIR)/:
	mkdir -p $(BINDIR)

$(OBJDIR):
	mkdir -p $(OBJDIR)

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

$(BINDIR)/libnanogui.so: $(BUILDDIR)/nanogui/libnanogui.so | $(BINDIR)/
	cp $< $@

$(OBJDIR)/nanogui/nanogui_resources.o: $(BUILDDIR)/nanogui/nanogui_resources.cpp
	mkdir -p $(dir $@)
	$(CC) -g -c -I lib -I lib/nanogui/include -I lib/nanogui/ext/eigen -I lib/nanogui/ext/glfw/include -I lib/nanogui/ext/nanovg/src -o $@ $(patsubst %.o,%.cpp,$(patsubst $(OBJDIR)%,$(BUILDDIR)%,$@))


$(BUILDDIR)/nanogui/libnanogui.so:
	git submodule update --init --recursive lib/nanogui
	mkdir -p $(BUILDDIR)/nanogui
	cd $(BUILDDIR)/nanogui && \
	cmake -DCMAKE_BUILD_TYPE=Debug -DNANOGUI_BUILD_PYTHON=OFF $(realpath $(LIBDIR)/nanogui) && \
	make